<?php ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit your Todolist</title>
    <link href="/style/style.css" rel="stylesheet">
</head>

<body>
    <h1> Edit your todo's</h1>
<div class="container">
        <?php
        // On pré-rempli le formulaire
        require 'connexion.php';
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $res = $connect->query("SELECT task_name FROM Tasks WHERE id = " . $id);
            $row = $res->fetch_array();
            if (isset($row['task_name'])) {
        ?>
        <form method="POST">
            <input type="text" value="<?php echo $row['task_name']; ?>" name="task">
            <input type="submit" value="Modify">
        </form>
        <?php } ?>
        <?php } ?>
        <?php
        // On met à jour les données
        if (isset($_POST['task'])) {
            $task = $_POST['task'];
            $requete ='UPDATE Tasks SET task_name= "'.$task .'" WHERE id ='.$id  ;       
            $results = $connect->query($requete);
            if ($results) {
                header("Location:index.php");
            } else {
                echo '<p class="error">Une erreur est survenue </p>';
            }
        }
        ?>
    </div>
</body>
</html>
