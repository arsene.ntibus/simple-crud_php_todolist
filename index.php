<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List (Simple CRUD)</title>
    <link rel="stylesheet" href="style/style.css">
</head>
<body>
    <h1> To-Do list</h1>
    <div class="container">
        <table> 
            <tr>
                <th>ID</th>
                <th>User Story</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <?php 
            require 'connexion.php';
            $tasks=$connect->query("SELECT * FROM Tasks");
            foreach ($tasks as $tasks) { ?>
                <tr>
                    <td>
                        <?php echo $tasks['id'] ?>
                    </td>
                    <td>
                        <?php echo $tasks['task_name'] ?>
                    </td>
                    <td>
                        <a class=" btn" href="edit.php?id=<?php echo $tasks['id']; ?>">Modify</a>
                        <a class="btn btn-danger" href="delete.php?id=<?php echo $tasks['id']; ?>"
                            onclick="return confirm('Êtes vous sûr de vouloir supprimer la tâche: <?php echo $task['task']; ?> ?')">Delete</a>
                    </td>
                </tr>
                <?php } ?>
            </table>

        </table>
    </div>
</body>
</html>